Source: gatk-bwamem
Section: java
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Pierre Gruet <pgt@debian.org>
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               gradle-debian-helper,
               libsimde-dev,
               maven-repo-helper,
               testng <!nocheck>,
               zlib1g-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/gatk-bwamem
Vcs-Git: https://salsa.debian.org/med-team/gatk-bwamem.git
Homepage: https://github.com/broadinstitute/gatk-bwamem-jni/
Rules-Requires-Root: no

Package: libgatk-bwamem-java
Architecture: all
Depends: libgatk-bwamem-jni (>= ${source:Version}),
         libgatk-bwamem-jni (<< ${source:Version}.1~),
         ${java:Depends},
         ${misc:Depends}
Suggests: bwa
Description: interface to call Heng Li's bwa mem aligner from Java code
 BWA (Burrows-Wheeler Aligner) is a software package for mapping low-divergent
 sequences against a large reference genome, such as the human genome. It is
 written in C.
 .
 gatk-bwamem provides a Java library and a shared library to allow one to use
 BWA from Java code.
 .
 This package contains the Java library.

Package: libgatk-bwamem-jni
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: interface to call Heng Li's bwa mem aligner from Java code (jni)
 BWA (Burrows-Wheeler Aligner) is a software package for mapping low-divergent
 sequences against a large reference genome, such as the human genome. It is
 written in C.
 .
 gatk-bwamem provides a Java library and a shared library to allow one to use
 BWA from Java code.
 .
 This package contains the native library.
